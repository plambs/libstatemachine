#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "statemachine.h"

static int _step_init_handler(int nb_loop, void * data){
	printf("Step init\n");
	sleep(1);
	return 1;
}
static int _step_one_handler(int nb_loop, void * data){
	printf("Step One\n");
	sleep(1);
	return 1;
}
static int _step_two_handler(int nb_loop, void * data){
	printf("Step Two\n");
	sleep(1);
	return 1;
}

enum{
	STATE_INIT = 0,
	STATE_1,
	STATE_2
};

T_statemachine sm = {
	STATEMACHINE_INITIALIZER
	STATE (STATE_INIT) {
		STEP( 0, &_step_init_handler, STATE_1 )
		STEP( 1, &_step_one_handler,  STATE_2 )
	},
	STATE (STATE_1){
		STEP( 0, _step_one_handler,  STATE_2    )
		STEP( 1, _step_two_handler,  STATE_INIT )
	},
	STATE (STATE_2){
		STEP( 0, _step_two_handler,  STATE_INIT )
		STEP( 1, _step_init_handler,  STATE_1   )
	},
};

int main( void){
	int ret;
	printf( "Start simple Statemachine declaration\n");

	ret = statemachine_init(&sm, NULL);

	ret = statemachine_start(&sm);

	while(1){
		pause();
	}
	return 0;
}
